// ak-tabs symbols

const buttonContainer = Symbol();
const focusOnRender = Symbol();
const labelsContainer = Symbol();

// ak-tabs-tab symbols

const tabDropdownItem = Symbol();
const tabLabel = Symbol();

export {
  buttonContainer,
  focusOnRender,
  labelsContainer,
  tabDropdownItem,
  tabLabel,
};
