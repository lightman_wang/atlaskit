# Inline Dialog

The Inline Dialog is a container for secondary content and controls that are displayed on user request.

## Setup and install

```
npm install ak-inline-dialog
```

## Using the definition

The `ak-inline-dialog` package exports the InlineDialog [skate](https://github.com/skatejs/skatejs) component:

```
import InlineDialog from 'ak-inline-dialog';

const myDialog = new InlineDialog();
```
