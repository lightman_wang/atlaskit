/**
 * This event gets emitted when an item in the {@link MentionList} gets selected.
 *
 * @event MentionPicker#selected
 */
export const selected = 'selected';

/**
 * This event gets emitted after the {@link MentionList} has been rendered.
 *
 * @event MentionPicker#mentionListRendered
 */
export const mentionListRendered = 'mentionListRendered';
