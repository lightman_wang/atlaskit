export const selected = 'selected';
export const afterOpen = 'after-open';
export const afterClose = 'after-close';
export const item = Object.freeze({
  up: 'up',
  down: 'down',
  tab: 'tab',
});
export const trigger = Object.freeze({
  activated: 'activated',
});
