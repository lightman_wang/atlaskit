/**
* This event is emitted when the blanket is clicked.
* The emission of this event can be controlled by the {@link Blanket#clickable} property.
*
* @event Blanket#activate
*/
export const activate = 'activate';
