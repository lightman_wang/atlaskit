# Avatar

The Avatars represent users that are collaborating within the same application. You can use an avatar for projects, repositories and spaces within the Atlassian applications.



## Setup and install

```
npm install ak-avatar
```

## Using the component

The `ak-avatar` package exports the Avatar [skate](https://github.com/skatejs/skatejs) component:

```
import Avatar from 'ak-avatar';

const myAvatar = new Avatar();
```
