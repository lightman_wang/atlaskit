export const linkSelected = 'linkSelected';
export const createDrawerOpen = 'createDrawerOpen';
export const searchDrawerOpen = 'searchDrawerOpen';
export const open = 'open';
export const close = 'close';
export const openStateChanged = 'openStateChanged';
